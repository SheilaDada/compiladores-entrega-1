%{
package coolc.parser;

%}

%output  "Parser.java"
%language "Java"


%define parser_class_name "Parser"
%define public

%code {

    public static String getTokenName(int t) {
        return yytname_[t-255];
    }

}

%token T_UNKNOWN

%token  T_LPAREN "(" T_RPAREN ")"
%token  T_LBRACE  "[" T_RBRACE "]"
%token  T_SEMICOLON ";"
%token  T_COMMA ","
%token  T_LLLAVE  "{" T_RLLAVE "}"
%token  T_2PUNTOS ":"
%token T_PLUS "+"
%token T_LFLECHA "<-"
%token  T_DOT "."
%token  T_TIL "~"
%token  T_MENOR "<"
%token  T_MENOS "-"
%token  T_MENORIGUAL "<="
%token  T_MULTI "*"
%token  T_IGUAL "="
%token  T_DIV "/"
%token  T_FLECHA2 "=>"
%token  T_ARROBA "@"


%token <String> Id
%token <String> Type
%token <Int> INT
%token <String> String
%token <String> Boolean

%token  T_CLASS "class"
%token  T_ELSE "else"
%token  T_FALSE "false"
%token  T_FI "fi"
%token  T_IF "if"
%token  T_IN "in"
%token  T_INHERITS "inherits"
%token  T_ISVOID "isvoid"
%token  T_LET "let"
%token  T_LOOP "loop"
%token  T_POOL "pool"
%token  T_THEN "then"
%token  T_WHILE "while"
%token  T_CASE "case"
%token  T_ESAC "esac"
%token  T_NEW "new"
%token  T_OF "of"
%token  T_NOT "not"


%%

expr
    : INT
    | expr "+" expr
    | Id "(" param_list ")"
    | "[" matrix "]"
    | Id
    ;

param_list
    : /* empty */ 
    | param_list "," expr
    ;

matrix
    : vector
    | matrix ";" vector
    ;

vector
    : expr
    | vector "," expr
    ;

%%
