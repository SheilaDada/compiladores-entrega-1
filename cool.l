package coolc.parser;

%%

%{

    private Object yylval;

    public Object getLVal() {
        return yylval;
    }

    public Position getStartPos() {
        return new Position(yyline, yycolumn);
    }

    public Position getEndPos() {
        return new Position(yyline, yycolumn);
    }

    public void yyerror(String msg) {
        //throw new Exception();
    }

%}

%public
%class Lexer
%unicode
%line
%column


%ignorecase

InputCharacter = [^\r\n]
LineTerminator = \r|\n|\r\n

/* comments */
Comment = {Comment1} | {Comment2}

Comment1 = "(*" [^*] ~"*)" | "(*" "*"+ ")"

Comment2 = "--" {InputCharacter}* {LineTerminator}

/* end comments */

Id     = [a-z]+[a-z|A-Z|"_"|0-9]*

Type = [A-Z]+[a-z|A-Z|"_"|0-9]*

String = \"[^\"]*\"

/* keywords case insensitive*/

class = [C|c][L|l][A|a][S|s][S|s]
else = [E|e][L|l][S|s][E|e]
false = [f][A|a][L|l][S|s][E|e]
fi = [F|f][I|i]
if = [I|i][F|f]
in = [I|i][N|n]
inherits = [I|i][N|n][H|h][E|e][R|r][I|i][T|t][S|s]
isvoid = [I|i][S|s][V|v][O|o][I|i][D|d]
let = [L|l][E|e][T|t]
loop = [L|l][O|o][O|o][P|p]
pool = [P|p][O|o][O|o][L|l]
then = [T|t][H|h][E|e][N|n]
while = [W|w][H|h][I|i][L|l][E|e]
case = [C|c][A|a][S|s][E|e]
esac = [E|e][S|s][A|a][C|c]
new = [N|n][E|e][W|w]
of = [O|o][F|f]
not = [N|n][O|o][T|t]
true = [t][R|r][U|u][E|e]

Boolean = {true}|{false}

/* end keywords */



%x comment
%x multiline_comment

%implements Parser.Lexer
%int

%%
    [0-9]+  {   
                yylval = Integer.parseInt(yytext());
                return Parser.INT;
            }

    {class}     { return Parser.T_CLASS; }

    {else}     { return Parser.T_ELSE; }

    {fi}     { return Parser.T_FI; }

    {if}     { return Parser.T_IF; }

    {in}     { return Parser.T_IN; }

    {inherits}     { return Parser.T_INHERITS; }

    {isvoid}     { return Parser.T_ISVOID; }

    {let}     { return Parser.T_LET; }

    {loop}     { return Parser.T_LOOP; }

    {pool}     { return Parser.T_POOL; }

    {then}     { return Parser.T_THEN; }

    {while}     { return Parser.T_WHILE; }

    {case}     { return Parser.T_CASE; }

    {esac}     { return Parser.T_ESAC; }

    {new}     { return Parser.T_NEW; }

    {of}     { return Parser.T_OF; }

    {not}     { return Parser.T_NOT; }


    {Boolean}    {
                yylval = yytext();
                return Parser.Boolean; 
            }

    {Id}    {
                yylval = yytext();
                return Parser.Id; 
            }

    {Type}    {
                yylval = yytext();
                return Parser.Type; 
            }

    {String}    {
                yylval = yytext();
                return Parser.String; 
            }







    "+"		{ return Parser.T_PLUS; }


    "("     { return Parser.T_LPAREN; }
    ")"     { return Parser.T_RPAREN; }
    "["     { return Parser.T_LBRACE; }
    "]"     { return Parser.T_RBRACE; }    
    ";"     { return Parser.T_SEMICOLON; }
    ","     { return Parser.T_COMMA; }
    "{"     { return Parser.T_LLLAVE; }
    "}"     { return Parser.T_RLLAVE; }
    ":"     { return Parser.T_2PUNTOS; }
    "."     { return Parser.T_DOT; }
    "~"     { return Parser.T_TIL; }
    "<"     { return Parser.T_MENOR; }
    "-"     { return Parser.T_MENOS; }
    "<="     { return Parser.T_MENORIGUAL; }
    "*"     { return Parser.T_MULTI; }
    "="     { return Parser.T_IGUAL; }
    "/"     { return Parser.T_DIV; }
    "=>"     { return Parser.T_FLECHA2; }
    "<-"     { return Parser.T_LFLECHA; }
    "@"     { return Parser.T_ARROBA; }

    
    [ \t\r\n\f]+  { 
        // ignoramos el espacio en blanco 
    }


    {Comment} { 
        // ignoramos el comentario
    }

    .           { System.out.printf("Unknown character [%s]\n", yytext());
                       return Parser.T_UNKNOWN;    }
    
    <<EOF>> { return Parser.EOF; }
